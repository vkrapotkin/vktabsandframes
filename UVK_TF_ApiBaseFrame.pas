unit UVK_TF_ApiBaseFrame;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  XSuperObject,
  UVKCommonApi,
  UVK_TF_BaseFrame, FMX.Objects, FMX.LoadingIndicator, FMX.Layouts;

type
  TVK_TF_ApiBaseFrame = class(TVK_TF_BaseFrame)
    layAnimation: TLayout;
    rAnimation: TRectangle;
    aniAnimation: TFMXLoadingIndicator;
    txtAnimation: TText;
  public
    FApi:TVKCommonAPI;
    procedure Load; override;
    procedure AnimationStart; override;
    procedure AnimationStop(const msg: string = ''); override;

    // data loading through API call:
    // 1) start animation
    // 2) call API (thread)
    // 3) looping call of AddDataitem to put data in adapter (thread)
    // 4) resetview
    // 5) stop animation
    procedure LoadData(); virtual;

    //  *************************************
    //  *** to override in ancestors ***
    //  *************************************
    procedure CloneApi(var AApi: TVKCommonAPI); virtual; abstract;
    // result - our data is in AApi.resultJson (thread!!)
    procedure CallApi(AApi: TVKCommonAPI); virtual;
    // clear adapter data before loading (thread!!)
    procedure ClearData; virtual;
    // create one data item from json and put it to adapter
    procedure AddDataItem(item: ISuperObject); virtual; abstract;
    // loaded data can be post processed here
    procedure DataLoaded(AApi: TVKCommonAPI); virtual;
    // we should use Adapter inside
    // so adapter is type
    procedure ResetView; virtual;
    //  *************************************
    //  *************************************
    //  *************************************
  end;

var
  asdApiBaseFrame: TVK_TF_ApiBaseFrame;

implementation

uses
  system.threading, FMX.Ani,
  UVKMessageDialog;

{$R *.fmx}

{ TVK_TF_ApiBaseFrame }

procedure TVK_TF_ApiBaseFrame.AnimationStart;
begin
  aniAnimation.Enabled := true;
  aniAnimation.Visible := true;
  aniAnimation.Active := True;
  txtAnimation.Text := '';
  layAnimation.Height := 0;
  TAnimator.AnimateFloat(layAnimation, 'Height', 50);
end;

procedure TVK_TF_ApiBaseFrame.AnimationStop(const msg: string);
begin
  aniAnimation.Enabled := false;
  aniAnimation.Visible := False;
  aniAnimation.Active := false;
  txtAnimation.Text := msg;
  if msg='' then
    TAnimator.AnimateFloat(layAnimation, 'Height', 0);
end;

procedure TVK_TF_ApiBaseFrame.CallApi(AApi: TVKCommonAPI);
begin
  // something like
  // AApi.Method(Arg1,Arg2);
end;

procedure TVK_TF_ApiBaseFrame.ClearData;
begin
//  *** uncomment in ancestors ***
//  Adapter.List.Clear;
end;

procedure TVK_TF_ApiBaseFrame.DataLoaded(AApi: TVKCommonAPI);
begin
  // loaded data can be postprocessed here
end;

procedure TVK_TF_ApiBaseFrame.Load;
begin
  TThread.ForceQueue(nil, procedure begin
    LoadData;
  end);
end;

procedure TVK_TF_ApiBaseFrame.LoadData;
begin
  AnimationStart();

  TTask.Run(
    procedure
    var
      a: ISuperArray;
      I: Integer;
      vApi: TVKCommonAPI;
    begin
      CloneApi(vApi);
      try
        CallApi(vApi);
        if Self.PARENT=nil then
          Exit;
        if (vApi.Err.Code = 0) then
        begin
          a := vApi.ResultJson.A['data'];
          ClearData;
          for I := 0 to a.Length - 1 do
          begin
            if Self.PARENT=nil then
              Exit;
            AddDataItem(a.O[I]);
          end;
          DataLoaded(vApi);
        end;

        TThread.Synchronize(nil,
          procedure
          begin
            if (vApi.Err.Code <> 0) then
            begin
              AnimationStop(vApi.Err.Msg);
              TVKMessageDialog.ShowDialog(Self, vapi.err.Msg);
            end
            else
            begin
              AnimationStop('');
              ResetView();
            end;
          end);
      finally
        vApi.free;
        vApi := nil;
      end;
    end);
end;

procedure TVK_TF_ApiBaseFrame.ResetView;
begin
//  *** uncomment in ancestors ***
//  Adapter.ResetView();
end;

end.
