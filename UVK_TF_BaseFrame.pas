unit UVK_TF_BaseFrame;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Effects, FMX.Filter.Effects, FMX.ImgList,
  FMX.Controls.Presentation, System.ImageList, System.Generics.Collections,
  XSuperObject,
  UVK_TF_TabManager,
  UVKCommonAPI
  ;

type

  TVK_TF_BaseFrame = class(TFrame)
  private
  public
    // controls whether the container window shows the toolbar
    ShowToolbar: Boolean;
    // action list for container's toolbar's context menu
    Actions:TArray<string>;

    [Weak]MgrItem: TVK_TF_TabMgrItem;

    constructor Create(AOwner: TComponent); override;
    function PopCount():integer; virtual;
    procedure ExecuteMenuAction(Sender:TObject); virtual;
    procedure Load; virtual;
    procedure Save; virtual;
    procedure AnimationStart; virtual;
    procedure AnimationStop(const msg:string=''); virtual;
  end;

var
  asdBaseFrame:TVK_TF_BaseFrame;

implementation
uses System.threading, FMX.Menus,
  UVKMessageDialog;

{$R *.fmx}

{ TskoBaseFrame }

procedure TVK_TF_BaseFrame.AnimationStart;
begin

end;

procedure TVK_TF_BaseFrame.AnimationStop(const msg:string='');
begin

end;

procedure TVK_TF_BaseFrame.Load;
begin
end;

function TVK_TF_BaseFrame.PopCount: integer;
begin
  result := 1;
end;

procedure TVK_TF_BaseFrame.Save;
begin
end;


constructor TVK_TF_BaseFrame.Create(AOwner: TComponent);
begin
  inherited;
//  *** set needed values in ancestors ***
//  ShowToolbar := true;
//  Actions := ['Send file', 'Delete files', ... ];
end;

procedure TVK_TF_BaseFrame.ExecutemenuAction(Sender: TObject);
//  *** use in ancestors this way ***
//var
//  mi:TMenuItem absolute Sender;
begin
//  if mi.Text='action name string' then
//  begin
//
//  end;
end;


end.
