unit UVK_TF_ContainerFrame;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.Edit, FMX.TabControl, System.Actions,
  FMX.ActnList, FMX.StdCtrls, System.Generics.Collections,
  System.ImageList, FMX.ImgList, FMX.Ani, FMX.Effects,
  FMX.Layouts, FMX.ListBox, FMX.Filter.Effects, FMX.ImageSlider, system.messaging,
  UVK_TF_TabManager,
  UVK_TF_BaseFrame,
  FMX.Menus
  ;

type

  TVK_TF_ContainerFrame = class(TVK_TF_BaseFrame)
    tbc1: TTabControl;
    pnlBkg: TPanel;
    ilToolbar: TImageList;
    tlb1: TToolBar;
    btnBack: TSpeedButton;
    glyBack: TGlyph;
    ln1: TLine;
    lblTitle: TLabel;
    rectShadow1: TRectangle;
    sb1: TVertScrollBox;
    MainLayout: TLayout;
    bDotsMenu: TSpeedButton;
    glDotMenu: TGlyph;
    lbDotMenu: TListBox;
    ShadowEffect1: TShadowEffect;
    li1: TListBoxItem;
    procedure btnBackClick(Sender: TObject);
    procedure bDotsMenuClick(Sender: TObject);
  private
    FPmBkgLayout:TLayout;
    procedure LayClick(Sender: TObject);
    procedure OnPopupMenuClick(Sender: TObject);
  public
    // inside GetTabMgr() we create appropriate Tab manager "on demand"
    function GetTabMgr(): TVK_TF_TabManager<TVK_TF_TabMgrItem>; virtual;
    procedure FreeTabMgr(); virtual; abstract;
    procedure OnBeforeChangeindex(Sender:TObject; IndexFrom, IndexTo:integer); virtual;
    procedure OnChangeindex(Sender:TObject; IndexFrom, IndexTo:integer); virtual;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;


implementation

uses
  UVK_TF_ApiBaseFrame;

{$R *.fmx}

procedure TVK_TF_ContainerFrame.LayClick(Sender: TObject);
begin
  FPmBkgLayout.Visible := False;
end;

procedure TVK_TF_ContainerFrame.OnPopupMenuClick(Sender:TObject);
begin
  (GetTabMgr.ActiveItem.Frame as TVK_TF_BaseFrame).ExecuteMenuAction(Sender);
  FPmBkgLayout.Visible := False;
end;

procedure TVK_TF_ContainerFrame.bDotsMenuClick(Sender: TObject);
var i:integer;
  li:TListBoxItem;
const
  ITEM_HEIGHT=35;
begin
  if FPmBkgLayout=NIL then
  begin
    FPmBkgLayout:=TLayout.Create(nil);
    FPmBkgLayout.Parent := Self;
    FPmBkgLayout.HitTest := true;
    FPmBkgLayout.OnClick := LayClick;
    FPmBkgLayout.SetBounds(0,0,Width,Height);
    lbDotMenu.Parent := FPmBkgLayout;
    lbDotMenu.Position.X := Width - lbDotMenu.Width - 30;
    lbDotMenu.Position.Y := 20;
  end;
  FPmBkgLayout.Visible := true;

  lbDotMenu.Clear;
  if (getTabMgr.ActiveItem <> nil)and(getTabMgr.ActiveItem.Frame<>NIL) then
  begin
    for i:=0 to Length((getTabMgr.ActiveItem.Frame as TVK_TF_BaseFrame).Actions)-1 do
    begin
      li := TListBoxItem.Create(self);
      li.Text := (getTabMgr.ActiveItem.Frame as TVK_TF_BaseFrame).Actions[i];
      li.Font.Size := 16;
      li.Height := ITEM_HEIGHT;
      li.OnClick := OnPopupMenuClick;
      li.StyleLookup := 'listboxitemlabel';
      lbDotMenu.AddObject(li);
    end;
    lbDotMenu.Visible := True;
    lbDotMenu.Height := Length((getTabMgr.ActiveItem.Frame as TVK_TF_BaseFrame).Actions) * ITEM_HEIGHT +10;
  end;
end;

procedure TVK_TF_ContainerFrame.btnBackClick(Sender: TObject);
var
  mgr : TVK_TF_TabManager<TVK_TF_TabMgrItem>;
begin
  mgr := GetTabMgr;
  mgr.Pop(  (mgr.ActiveItem.Frame as TVK_TF_BaseFrame).PopCount() );
end;

constructor TVK_TF_ContainerFrame.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  tbc1.TabPosition := TTabPosition.None;
  lbDotMenu.Visible := false;
end;

destructor TVK_TF_ContainerFrame.Destroy;
begin
  FreeTabMgr();
  inherited Destroy;
end;


function TVK_TF_ContainerFrame.GetTabMgr: TVK_TF_TabManager<TVK_TF_TabMgrItem>;
begin
  result := nil;
//  example
//  if L1TabMgr=NIL then
//  begin
//    L1TabMgr := TL1TabManager.Create(tbc1, tlb1);
//    L1TabMgr.OnBeforeChangeIndex := OnBeforeChangeindex;
//    L1TabMgr.OnChangeIndex := OnChangeindex;
//  end;
//  Result := TVK_TF_TabManager<TVK_TF_TabMgrItem>(L1TabMgr);
end;


procedure TVK_TF_ContainerFrame.OnBeforeChangeindex(Sender: TObject; IndexFrom, IndexTo:integer);
var
  tabMgr : TVK_TF_TabManager<TVK_TF_TabMgrItem>;
begin
  glyBack.Visible := False;
  btnBack.Enabled := False;
  tabMgr := getTabMgr();
  if (TabMgr.ActiveItem<>nil)and(TabMgr.ActiveItem.Frame<>nil) then
  begin
    (TabMgr.ActiveItem.Frame as TVK_TF_BaseFrame).AnimationStart;
    (TabMgr.ActiveItem.Frame as TVK_TF_BaseFrame).Save;
  end;
  if (TabMgr[IndexTo]<>NIL)and(TabMgr[IndexTo].Frame<>NIL) then
  begin
    if (TabMgr[IndexTo].Frame is TVK_TF_ApiBaseFrame) then
      TVK_TF_ApiBaseFrame(TabMgr[IndexTo].Frame).ClearData;
  end;
end;

procedure TVK_TF_ContainerFrame.OnChangeindex(Sender: TObject; IndexFrom, IndexTo:integer);
var
  tabMgr : TVK_TF_TabManager<TVK_TF_TabMgrItem>;
begin
  tabMgr := getTabMgr();
  if (TabMgr.ActiveItem<>nil)and(TabMgr.ActiveItem.Frame<>nil) then
    (TabMgr.ActiveItem.Frame as TVK_TF_BaseFrame).Load;
  glyBack.Visible := true;
  btnBack.Enabled := true;
end;


end.

