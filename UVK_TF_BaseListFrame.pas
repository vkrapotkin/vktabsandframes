unit UVK_TF_BaseListFrame;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.ListView.Types, FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base, FMX.ListView, System.ImageList, FMX.ImgList,
  FMX.Layouts, FMX.Objects,
  FMX.LoadingIndicator, FMX.Effects, FMX.Filter.Effects,
  FMX.Controls.Presentation,
  XSuperObject,
  UVK_TF_BaseFrame,
  UVK_TF_ApiBaseFrame,
  UVKCommonAPI, FMX.Edit;


type
  TVK_TF_BaseListFrame = class(TVK_TF_ApiBaseFrame)
    lv1: TListView;
    eFilter: TEdit;
    tmrFilter: TTimer;
    btnApplyFilter: TSearchEditButton;
    procedure lv1UpdatingObjects(const Sender: TObject;
      const AItem: TListViewItem; var AHandled: Boolean);
    procedure txt1Click(Sender: TObject);
    procedure FrameKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure lv1PullRefresh(Sender: TObject);
    procedure lv1Resize(Sender: TObject);
    procedure eFilterTyping(Sender: TObject);
    procedure btnApplyFilterClick(Sender: TObject);
    procedure eFilterChange(Sender: TObject);
  private
  protected
  public
    // whether the filter works automatically by timer
    AutoFilter: Boolean;
    constructor Create(AOwner: TComponent); override;

  end;

var
 asdBaseListFrame : TVK_TF_BaseListFrame;


implementation

uses
  system.threading, FMX.ani;

{$R *.fmx}


procedure TVK_TF_BaseListFrame.btnApplyFilterClick(Sender: TObject);
begin
  ResetView;
  tmrFilter.Enabled := false;
end;

constructor TVK_TF_BaseListFrame.Create(AOwner: TComponent);
begin
  inherited;
  eFilter.Visible := false;
  tmrFilter.Enabled := False;
end;

procedure TVK_TF_BaseListFrame.eFilterChange(Sender: TObject);
begin
  ResetView;
  tmrFilter.Enabled := false;
end;

procedure TVK_TF_BaseListFrame.eFilterTyping(Sender: TObject);
begin
  if AutoFilter then
  begin
    tmrFilter.Enabled := false;
    tmrFilter.Enabled := true;
  end;
end;

procedure TVK_TF_BaseListFrame.FrameKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
begin
  if (KeyChar='R')
  and (Shift=[ssCtrl,ssAlt]) then
    LoadData;
end;


procedure TVK_TF_BaseListFrame.lv1PullRefresh(Sender: TObject);
begin
  LoadData;
end;

procedure TVK_TF_BaseListFrame.lv1Resize(Sender: TObject);
begin
//
end;

procedure TVK_TF_BaseListFrame.lv1UpdatingObjects(const Sender: TObject;
  const AItem: TListViewItem; var AHandled: Boolean);
begin
//  *** ����������������� � ����������� ***
//  Adapter.SetupContent(AItem);
//  Adapter.DoLayout(AItem);
//  AHandled := true;
end;


procedure TVK_TF_BaseListFrame.txt1Click(Sender: TObject);
begin
  AnimationStop('');
end;

end.
