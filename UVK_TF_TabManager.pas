unit UVK_TF_TabManager;

interface
uses
  System.Classes, System.Types, System.Generics.Collections, FMX.Forms,
  FMX.TabControl, FMX.StdCtrls, FMX.Types, System.SysUtils;

type
  TChangeIndexEvent = procedure (Sender:TObject; IndexFrom, IndexTo: Integer) of object;
  TFrameClass = class of FMX.Forms.TFrame;

  TVK_TF_TabMgrItem = class
  private
    FTitle: string;
  protected
    procedure SetTitle(const Value: string); virtual;
  public
    Active: Boolean;
//    [Weak]
    Frame:FMX.Forms.TFrame;
//    [Weak]
    tabItem: TTabItem;
    constructor Create(const ATitle:string; const ATab:TTabItem); virtual;
    destructor Destroy; override;
    function GetClassOfFrame():TFrameClass; virtual;
    procedure Update(force:Boolean=false); virtual; abstract;
    property Title: string read FTitle write SetTitle;
  end;

  TVK_TF_TabManager<T:TVK_TF_TabMgrItem> = class(TObjectlist<T>)
  private
    FTabsToDelete:TList<integer>;
    FAllowDuplicates: Boolean;
    FActiveIndex: integer;
    FPrevIndex: integer;
    FOnChangeIndex: TChangeIndexEvent;
    FOnBeforeChangeIndex: TChangeIndexEvent;
    procedure SetAllowDuplicates(const Value: Boolean);
    procedure SetActiveIndex(const Value: integer);
  public
    tabControl: TTabcontrol;
    toolbar: TToolbar;
    constructor Create(const ATabControl:TTabcontrol; const AToolbar:TToolBar);
    destructor Destroy; override;


    function Add(item:T):Integer; virtual;
    procedure Delete(index:integer); virtual;
    // ���� AllowDuplicates=false �� ���� ��� ������� ����� ���������� ������ � ������ ���
    // ���� �� �������, ������� �����
    function AddTab(const ATitle:string; GoImmediately:boolean=true):integer; virtual;
    // ������� ��������� ����� � ���, �� ������� �� ���������
    // ��������� �� �������������, ���� ����
    function ActiveItem():T;
    procedure Pop(N:integer=1);
    procedure UpdateToolbar();
    function ItemOfTitle(const ATitle:string):T;
    function IndexOfTitle(const ATitle:string):integer;
    procedure SetActiveByTitle(const ATitle:string);
    procedure OnAfterChangeIndex();

    property ActiveIndex: integer read FActiveIndex write SetActiveIndex;
    property AllowDuplicates: Boolean read FAllowDuplicates write SetAllowDuplicates;

    property OnBeforeChangeIndex:TChangeIndexEvent read FOnBeforeChangeIndex write FOnBeforeChangeIndex;
    property OnChangeIndex:TChangeIndexEvent read FOnChangeIndex write FOnChangeIndex;
  end;




implementation

uses
  UVK_TF_BaseFrame;

{ TskoFrameManager }

constructor TVK_TF_TabManager<T>.Create(const ATabControl: TTabcontrol;
  const AToolbar: TToolBar);
begin
  inherited Create(true);
  tabControl := ATabControl;
  toolbar := AToolbar;
  FAllowDuplicates := true;
  FTabsToDelete := TList<Integer>.Create;
end;


procedure TVK_TF_TabManager<T>.SetAllowDuplicates(const Value: Boolean);
begin
  FAllowDuplicates := Value;
end;

procedure TVK_TF_TabManager<T>.SetActiveByTitle(const ATitle:string);
var
  i:Integer;
begin
  i := IndexOfTitle(ATitle);
  if i=-1 then
    raise Exception.Create('������ 01478');
  ActiveIndex := i;
end;

procedure TVK_TF_TabManager<T>.SetActiveIndex(const value: integer);
var
  dir: TTabTransitionDirection;
  item1, item2:T;
begin
  if (Value<0)and(Value>=Count) then
    exit;

  if Assigned(FOnBeforeChangeIndex) then
    FOnBeforeChangeIndex(Self, FActiveIndex, Value);

  if (FActiveIndex>=0)and(FActiveIndex<>value) then
  begin
    item1 := Items[FActiveIndex];
    item1.active := false;
  end
  else
    item1 := nil;
  item2 := Items[Value];
  item2.Active := true;

  FPrevIndex := FActiveIndex;
  FActiveIndex := Value;

  //update ������� ������ ����� ���� ��� ��� �� ����
  // ��������� �� ������ ����� � OnAfterChangeindex
  // ������ ��� ������ ��� ���������� Settings.Mode !!!

//  if item1 <> nil then item1.Update;
  if item2 <> nil then item2.Update;

  UpdateToolbar;
  if tabControl.ActiveTab.Index=FActiveIndex then
  begin
    if Assigned(FOnChangeIndex) then
      FOnChangeIndex(Self, FPrevIndex, FActiveIndex);
    exit;
  end;

  if tabControl.ActiveTab.Index<FActiveIndex then
    dir := TTabTransitionDirection.Normal
  else
    dir := TTabTransitionDirection.Reversed;

  tabControl.SetActiveTabWithTransitionAsync(tabControl.Tabs[FActiveIndex],
              TTabTransition.Slide, dir,
              procedure
              begin
                OnAfterChangeIndex(); // ������ ��������� ������� FOnChangeIndex
              end);
end;

procedure TVK_TF_TabManager<T>.Pop(N:integer=1);
var
  i:integer;
begin
  // ��� ����� ������� ����, ��
  // � ActiveIndex ��� ���� �����,
  // ������� �������� ��������
  // � ������� � OnAfterChangeIndex() !
  if Count-1-n<0 then
    exit;

  ActiveIndex := Count-1-n;
  UpdateToolbar();
  for I := 0 to n-1 do
    FTabsToDelete.add(Count-1-i);
end;

procedure TVK_TF_TabManager<T>.Delete(index:integer);
begin
  inherited Delete(index);
  TabControl.Delete(index);
end;


destructor TVK_TF_TabManager<T>.Destroy;
begin
  FreeAndNil(FTabsToDelete);
  inherited;
end;

function TVK_TF_TabManager<T>.IndexOfTitle(const ATitle: string): integer;
var
  i: Integer;
begin
  for i := 0 to Count-1 do
  begin
    if Items[i].title=aTitle then
    begin
      Result := i;
      exit;
    end;
  end;
  result := -1;
end;

function TVK_TF_TabManager<T>.ItemOfTitle(const ATitle: string): T;
var
  i: Integer;
begin
  result := nil;
  i := IndexOfTitle(ATitle);
  if i<>-1 then
    result := Items[i];
end;

procedure TVK_TF_TabManager<T>.OnAfterChangeIndex();
begin
  while FTabsToDelete.Count>0 do
  begin
    Delete(FTabsToDelete.first);
    FTabsToDelete.Delete(0);
  end;
  if Assigned(FOnChangeIndex) then
    FOnChangeIndex(Self, FPrevIndex, FActiveIndex);
end;

function TVK_TF_TabManager<T>.ActiveItem: T;
begin
  result := nil;
  if (ActiveIndex<0)or(ActiveIndex>=Count) then
    exit;
  result := Items[ActiveIndex];
end;

function TVK_TF_TabManager<T>.Add(item: T): Integer;
begin
  result := inherited Add(item);
end;

function TVK_TF_TabManager<T>.AddTab(const ATitle:string; GoImmediately:boolean=true):integer;
var
  item : T;
  tab:TTabItem;
begin
  tab:=tabControl.Add();
  tab.Name := tabControl.name+'tab'+ IntToStr(count);

  item := T.Create(ATitle, tab);
  result := add(item);


  if GoImmediately then
    SetActiveIndex(result);
end;

procedure TVK_TF_TabManager<T>.UpdateToolbar();
var lbl : TLabel;
  btnBack, btnMenu: TSpeedButton;
  i: Integer;
begin
  if toolbar<>nil then
  begin
    toolbar.Visible := (ActiveItem.frame<>nil) and (ActiveItem.frame as TVK_TF_BaseFrame).ShowToolbar;
    lbl := nil;
    btnMenu := nil;
    btnBack := nil;
    for i := 0 to toolbar.ChildrenCount-1 do
    begin
      if AnsiSameText(ToolBar.Children[i].Name, 'lblTitle') then
        lbl := ToolBar.Children[i] as TLabel
      else
      if AnsiSameText(ToolBar.Children[i].Name, 'bDotsMenu') then
        btnMenu := ToolBar.Children[i] as TSpeedButton
      else
      if AnsiSameText(ToolBar.Children[i].Name, 'btnBack') then
        btnBack := ToolBar.Children[i] as TSpeedButton;
    end;
    if lbl<>NIL then
      lbl.Text := items[FActiveIndex].Title;
    if btnBack<>nil then
      btnBack.Visible := ActiveIndex>0;
    if (btnMenu<>nil) then
      btnMenu.Visible := Length( (ActiveItem.frame as TVK_TF_BaseFrame).Actions )>0;
  end;
end;

{ TskoFrameMgrItem }

constructor TVK_TF_TabMgrItem.Create(const ATitle: string; const ATab: TTabItem);
begin
  Title := Atitle;
  tabItem := ATab;
end;

destructor TVK_TF_TabMgrItem.Destroy;
begin
  if Frame<>nil then
  begin
    if Frame.Parent<>nil then
      Frame.Parent.RemoveObject(Frame);
    Frame.Free;
    Frame:=NIL;
  end;
  inherited;
end;

function TVK_TF_TabMgrItem.GetClassOfFrame: TFrameClass;
begin
  result := TFrame;
end;

procedure TVK_TF_TabMgrItem.SetTitle(const Value: string);
begin
  FTitle := Value;
end;



end.
